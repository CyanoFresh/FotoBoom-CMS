<?php

// Paths constants
define('ROOT', dirname(__FILE__));
define('APP', ROOT . '/app');

// Start session
session_start();

// Require application class and config.php
require APP . '/core/app.php';
require APP . '/config/config.php';
require APP . '/config/settings.php';

// Run application
core\App::getInstance($config, $settings);
