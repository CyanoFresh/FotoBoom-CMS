<?php

namespace core;

use helpers\G;

class Router {

    /**
     * Array with routes
     *
     * @var array
     */
    private $routes = array();

    /**
     * Array with regex aliases
     *
     * @var array
     */
    private $aliases = array(
        ':any' => '[^/]+',
        ':num' => '[0-9]+',
        ':all' => '.*'
    );

    /**
     * Callbacks
     */
    public $controller = 'home';
    public $action     = 'index';
    public $params     = array();

    /**
     * Set routes array
     *
     * @param array $routes
     */
    public function __construct($routes) {
        $this->routes = $routes;
    }

    /**
     * Get callback and run it
     */
    public function run() {
        if (!empty($_GET['url'])) {
            $url = trim(G::get('url'), '/');

            if ($url == 'portfolio/17') {
                header('Location: /portfolio/5', true, 301);
                exit;
            } elseif ($url == 'portfolio/19') {
                header('Location: /portfolio/1', true, 301);
                exit;
            } elseif ($url == 'articles/13') {
                header('Location: /articles/4', true, 301);
                exit;
            }

            $this->dispatch($url);
        }

        $controller = 'controllers\\' . $this->controller;

        if (class_exists($controller)) {
            $controller = new $controller();
        }

        $action = strtolower($_SERVER['REQUEST_METHOD']) . ucwords($this->action);

        if (is_callable(array($controller, $action))) {
            call_user_func_array(array($controller, $action), $this->params);
        } elseif (is_callable(array($controller, 'any' . ucwords($this->action)))) {
            call_user_func_array(array($controller, 'any' . ucwords($this->action)), $this->params);
        } else {
            $this->error404();
        }
    }

    public function dispatch($url) {
        if (array_key_exists($url, $this->routes)) {
            $this->controller = $this->routes[$url]['controller'];
            $this->action     = $this->routes[$url]['action'];
            return;
        }

        $alias = array_keys($this->aliases);
        $regex = array_values($this->aliases);

        foreach ($this->routes as $pattern => $callback) {
            if (strpos($pattern, ':')) {
                $pattern = str_replace($alias, $regex, $pattern);
            }

            $matches = array();
            if (preg_match('#^' . $pattern . '$#', $url, $matches)) {
                $this->setController($callback, $matches);
                $this->setFunction($callback, $matches);
                $this->setParams($callback, $matches);
                return;
            }
        }
    }

    private function setController($callback, $matches) {


        if (is_int($callback['controller'])) {
            $this->controller = $matches[$callback['controller']];
        } else {
            $this->controller = $callback['controller'];
        }
    }

    private function setFunction($callback, $matches) {
        if (!isset($callback['action'])) {
            return;
        }

        if (is_int($callback['action'])) {
            $this->action = $matches[$callback['action']];
        } else {
            $this->action = $callback['action'];
        }
    }

    private function setParams($callback, $matches) {
        if (!isset($callback['params'])) {
            return;
        }

        if (is_int($callback['params'])) {
            if (isset($matches[$callback['params']])) {
                $this->params = explode('/', $matches[$callback['params']]);
            }
        }
    }

    private function error404() {
        $error = new Controller();
        $error->error404();
        exit;
    }

}
