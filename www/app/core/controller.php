<?php

namespace core;

use helpers\G,
    helpers\Message;

class Controller {

    protected $view;
    protected $model;

    public function __construct() {
        // Init the view
        $this->view = View::getInstance();

        // Set default vars to render in template. ONLY IN UPPERCASE!
        $this->view->addData(array(
            'URL' => App::$config['system']['base_url'],
            'THEME' => App::$config['system']['base_url'] . '/theme',
            'LOGIN' => G::session('login'),
            'CONFIG' => App::$settings,
            'MSG' => Message::get()
        ));
    }

    protected function checkLogin() {
        return (bool) G::session('login');
    }

    public function error404() {
        header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found', true, 404);

        $this->view->render('errors/404');
        exit;
    }

    public function error401() {
        header($_SERVER['SERVER_PROTOCOL'] . ' 401 Unauthorized', true, 401);

        $this->view->render('errors/401');
        exit;
    }

    public function error500() {
        header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);

        $this->view->render('errors/500');
        exit;
    }

    public function redirect($url, $base = true) {
        if ($base) {
            $url = App::$config['system']['base_url'] . $url;
        }
        header('Location: ' . $url);
    }

}
