<?php

namespace core;

class App {

    private static $_instance = null;
    public static $settings   = array();
    public static $config     = array();

    public static function getInstance($config, $settings) {
        if (is_null(self::$_instance)) {
            self::$_instance = new self($config, $settings);
        }
        return self::$_instance;
    }

    private function __construct($config, $settings) {
        // Set config
        self::$settings = $settings;
        self::$config = $config;
        // Register autoloader
        spl_autoload_register(array('self', 'loader'));
        // Init router
        $router = new Router(self::$config['routes']);
        $router->run();
    }

    public static function loader($class) {
        $class = str_replace('\\', '/', strtolower($class));
        $file  = ROOT . '/app/' . $class . '.php';

        if (file_exists($file)) {
            require $file;
        } else {
            return false;
        }
    }

}
