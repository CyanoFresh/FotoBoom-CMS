<?php

namespace core;

use helpers\Database;

abstract class Model {

    protected $db;

    public function __construct() {
        $this->db = Database::getInstance(App::$config['db']);
    }

}
