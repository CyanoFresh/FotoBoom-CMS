<?php

namespace core;

/**
 * View class
 */
class View {

    /**
     * View instance
     *
     * @var instance
     */
    private static $_instance = null;

    /**
     * Template engine object
     *
     * @var object
     */
    private $tpl = null;

    /**
     * Data to render
     *
     * @var assoc array
     */
    private $data = array();

    /**
     * Get view instance
     *
     * @return instance
     */
    public static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Register template engine
     */
    public function __construct() {
        require APP . '/libs/Twig/Autoloader.php';
        \Twig_Autoloader::register();
        $loader = new \Twig_Loader_Filesystem(ROOT . '/theme');

        $this->tpl = new \Twig_Environment($loader, array(
            'cache' => APP . '/tmp/cache/twig',
            'debug' => true
        ));
    }

    /**
     * Add var to render data
     *
     * @param string $var
     * @param mixed $value
     */
    public function add($var, $value) {
        $this->data[$var] = $value;
    }

    /**
     * Add vars array to render data
     *
     * @param array $data
     */
    public function addData(array $data) {
        $this->data = $this->data + $data;
    }

    /**
     * Render the template
     *
     * @param string $template
     * @param array $data
     */
    public function render($template, $data = array()) {
        $template = $template . App::$config['system']['tpl_ext'];
        $data     = $this->data + $data;

        echo $this->tpl->render($template, $data);
    }

}
