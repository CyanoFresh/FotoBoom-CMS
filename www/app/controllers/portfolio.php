<?php

namespace controllers;

use core\Controller,
    helpers\Message;

class Portfolio extends Controller {

    public function __construct() {
        parent::__construct();
        $this->model = new \models\Portfolio();
        $this->view->add('page', 'portfolio');
    }

    public function anyIndex() {
        $albums = $this->model->albums();

        $this->view->render('portfolio/index', array(
            'albums' => $albums
        ));
    }

    public function anySingle($id) {
        $album = $this->model->album($id);

        if (!$album) {
            $this->error404();
        }

        $photos = $this->model->getPhotos($id);

        $this->view->render('portfolio/single', array(
            'album' => $album,
            'photos' => $photos
        ));
    }

    public function postAddAlbum() {
        if (!$this->checkLogin()) {
            $this->error401();
        }

        $msg = $this->model->addAlbum();
        Message::set($msg);

        $this->redirect('/portfolio');
    }

    public function postSaveAlbum($id) {
        if (!$this->checkLogin()) {
            $this->error401();
        }

        $msg = $this->model->saveAlbum($id);
        Message::set($msg);

        $this->redirect('/portfolio/' . $id);
    }

    public function getDelAlbum($id) {
        if (!$this->checkLogin()) {
            $this->error401();
        }

        $msg = $this->model->delAlbum($id);
        Message::set($msg);

        $this->redirect('/portfolio');
    }

    public function postSavePhoto() {
        if (!$this->checkLogin()) {
            $this->error401();
        }

        if ($this->model->savePhoto()) {
            header('HTTP 200 Ok', true, 200);
        }
    }

    public function postDropPhoto($album) {
        if (!$this->checkLogin()) {
            $this->error401();
        }

        header('Content-type: application/json');
        echo json_encode($this->model->dropPhoto($album));
    }

    public function postDelPhoto($album) {
        if (!$this->checkLogin()) {
            $this->error401();
        }

        $this->model->delPhoto($album);
    }

    public function postOrder($id) {
        if (!$this->checkLogin()) {
            $this->error401();
        }

        $this->model->order($id);
    }

}
