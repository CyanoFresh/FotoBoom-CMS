<?php

namespace controllers;

use core\Controller,
    helpers\Message;

class Prices extends Controller {

    public function __construct() {
        parent::__construct();
        $this->model = new \models\Prices();
        $this->view->add('page', 'prices');
    }

    public function anyIndex() {
        $prices = $this->model->prices();

        $this->view->render('prices/index', array(
            'prices' => $prices
        ));
    }

    public function anySingle($id) {
        $id    = (int) $id;
        $price = $this->model->price($id);

        if (!$price) {
            $this->error404();
        }

        $this->view->render('prices/single', array(
            'price' => $price
        ));
    }

    public function postAdd() {
        if (!$this->checkLogin()) {
            $this->error401();
        }

        $msg = $this->model->add();
        Message::set($msg);

        $this->redirect('/prices');
    }

    public function postSave($id) {
        if (!$this->checkLogin()) {
            $this->error401();
        }

        $msg = $this->model->save($id);
        Message::set($msg);

        $this->redirect('/prices/' . $id);
    }

    public function getDelete($id) {
        if (!$this->checkLogin()) {
            $this->error401();
        }

        $msg = $this->model->delete($id);
        Message::set($msg);

        $this->redirect('/prices');
    }

}
