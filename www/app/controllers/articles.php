<?php

namespace controllers;

use core\Controller,
    helpers\Message;

class Articles extends Controller {

    public function __construct() {
        parent::__construct();
        $this->model = new \models\Articles();
        $this->view->add('page', 'articles');
    }

    public function anyIndex() {
        $articles = $this->model->articles();

        $this->view->render('articles/index', array(
            'articles' => $articles
        ));
    }

    public function anySingle($id) {
        $id = (int) $id;

        $article = $this->model->single($id);

        if (!$article) {
            $this->error404();
        }

        $this->view->render('articles/single', array(
            'article' => $article
        ));
    }

    public function postAdd() {
        if (!$this->checkLogin()) {
            $this->error401();
        }

        $msg = $this->model->add();
        Message::set($msg);

        $this->redirect('/articles');
    }

    public function postSave($id) {
        if (!$this->checkLogin()) {
            $this->error401();
        }

        $msg = $this->model->save($id);
        Message::set($msg);

        $this->redirect('/articles/' . $id);
    }

    public function getDelete($id) {
        if (!$this->checkLogin()) {
            $this->error401();
        }

        $msg = $this->model->delete($id);
        Message::set($msg);

        $this->redirect('/articles');
    }

}
