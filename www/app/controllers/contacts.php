<?php

namespace controllers;

use core\Controller,
    helpers\Message,
    helpers\Captcha;

class Contacts extends Controller {

    public function __construct() {
        parent::__construct();
        $this->model = new \models\Contacts();
        $this->view->add('page', 'contacts');
    }

    public function anyIndex() {
        $this->view->render('contacts/index', array(
            'captcha' => Captcha::html()
        ));
    }

    public function postSend() {
        $msg = $this->model->send();
        Message::set($msg[0]);

        $this->redirect('/contacts');
    }

    public function postXhrSend() {
        header('Content-type: application/json');

        $respond = $this->model->send();

        echo json_encode($respond);
    }

}
