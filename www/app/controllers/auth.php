<?php

namespace controllers;

use core\Controller,
    helpers\Csrf,
    helpers\Message;

class Auth extends Controller {

    public function __construct() {
        parent::__construct();
        $this->model = new \models\Auth();
    }

    public function anyIndex() {
        if ($this->checkLogin()) {
            $this->redirect('/');
        }

        $this->view->render('auth/index', array(
            'CSRF' => Csrf::token()
        ));
    }

    public function postLogin() {
        if ($this->checkLogin()) {
            $this->redirect('/');
        }

        $msg = $this->model->login();
        if ($msg) {
            Message::set($msg);
            $this->redirect('/auth');
        } else {
            $this->redirect('/');
        }
    }

    public function anyLogout() {
        $this->model->logout();
        $this->redirect('/');
    }

}
