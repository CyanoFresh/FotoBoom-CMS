<?php

namespace controllers;

use core\Controller;

class Home extends Controller {

    private $reviews_model = null;

    public function __construct() {
        parent::__construct();
        $this->view->add('page', 'home');
        $this->reviews_model = new \models\Reviews();
    }

    public function anyIndex() {
        $portfolio_model = new \models\Portfolio();
        $prices_model    = new \models\Prices();

        $albums  = $portfolio_model->mainAlbums();
        $prices  = $prices_model->mainPrices();
        $reviews = $this->reviews_model->reviews();

        $this->view->render('home/index', array(
            'albums'  => $albums,
            'prices'  => $prices,
            'reviews' => $reviews,
        ));
    }

    public function postAddReview() {
        if (!$this->checkLogin()) {
            $this->error401();
        }

        $this->reviews_model->addReview();

        $this->redirect('/');
    }

}
