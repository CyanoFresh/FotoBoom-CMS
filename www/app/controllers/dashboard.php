<?php

namespace controllers;

use core\Controller,
    helpers\Message;

class Dashboard extends Controller {

    public function __construct() {
        parent::__construct();
        $this->model = new \models\Dashboard;
        $this->view->add('page', 'dashboard');
    }

    public function anyIndex() {
        $this->view->render('dashboard/index');
    }

    public function postSave() {
        if (!$this->checkLogin()) {
            $this->error401();
        }

        $msg = $this->model->save();
        Message::set($msg);

        $this->redirect('/dashboard');
    }

    public function postAddAdmin() {
        if (!$this->checkLogin()) {
            $this->error401();
        }

        $msg = $this->model->addAdmin();
        Message::set($msg);

        $this->redirect('/dashboard');
    }

    public function getCache() {
        if (!$this->checkLogin()) {
            $this->error401();
        }

        $msg = $this->model->cache();
        Message::set($msg);

        $this->redirect('/dashboard');
    }

}
