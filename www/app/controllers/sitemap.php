<?php

namespace controllers;

use core\Controller;

class Sitemap extends Controller {

    public function anyIndex() {
        $portfolio_m = new \models\Portfolio;
        $albums      = $portfolio_m->albums();

        $prices_m = new \models\Prices;
        $prices   = $prices_m->prices();

        $articles_m = new \models\Articles;
        $articles   = $articles_m->articles();

        $this->view->render('sitemap/index', array(
            'albums' => $albums,
            'prices' => $prices,
            'articles' => $articles
        ));
    }

}
