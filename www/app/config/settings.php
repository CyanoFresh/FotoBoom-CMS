<?php

$settings = array(
    'home_title' => 'Главная',
    'home_description' => 'Описание главной',
    'home_keywords' => 'Ключ слова главной',
    'portfolio_title' => 'Портфолио',
    'portfolio_description' => 'Описание портфолио',
    'portfolio_keywords' => 'Ключ слова',
    'prices_title' => 'Цены',
    'prices_description' => 'Описание',
    'prices_keywords' => 'Ключ слова',
    'articles_title' => 'Статьи',
    'articles_description' => 'Описание',
    'articles_keywords' => 'Ключ слова',
    'contacts_title' => 'Контакты',
    'contacts_description' => 'Описание',
    'contacts_keywords' => 'Ключ слова',
);
