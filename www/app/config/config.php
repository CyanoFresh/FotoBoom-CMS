<?php

$config = array(
    'system' => array(
        'sec_code' => '645bb0d07421ca2561e45a088f28fe99',
        'base_url' => 'http://localhost/fotoboom/www',
        'tpl_ext' => '.html',
        'publickey' => '6LcMuvcSAAAAAIWRKcXCHfVAUg7hZXJ8hN8y3-kx',
        'privatekey' => '6LcMuvcSAAAAAOPVoIo7GvblM0Bqg6mdkHu3O8Dc',
    ),
    'db' => array(
        'host' => 'localhost',
        'user' => 'root',
        'pass' => '',
        'db' => 'fotoboom',
    ),
    'routes' => array(
        '(:any)' => array(
            'controller' => 1,
        ),
        '(:any)/(:num)' => array(
            'controller' => 1,
            'action' => 'single',
            'params' => 2,
        ),
        '(:any)/(:any)' => array(
            'controller' => 1,
            'action' => 2,
        ),
        '(:any)/(:any)/(:num)' => array(
            'controller' => 1,
            'action' => 2,
            'params' => 3,
        ),
    )
);
