<?php

namespace models;

use core\Model,
    helpers\G,
    helpers\Captcha;

class Contacts extends Model {

    public function send() {
        if (!Captcha::valid()) {
            return array(
                'Символы введены неверно. Попробуйте снова, данные сохранились, не беспокойтесь ;)',
                true
            );
        }

        $message = 'На Вашем сайте была заполнена контактная форма. Содержимое формы:' . "\n\n"
            . '##########################' . "\n"
            . 'Хочу: ' . G::post('action') . "\n"
            . 'Имя: ' . G::post('name') . "\n"
            . 'Email: ' . G::post('email') . "\n"
            . 'Телефон: ' . G::post('phone') . "\n"
            . 'Сообщение: ' . G::post('msg') . "\n"
            . 'Как нашли нас: ' . G::post('found') . "\n"
            . 'Ответить на: ' . G::post('replyTo') . "\n"
            . '##########################';

        mail('info@fotoboom.net', 'Заполнена контактная форма', $message);

        if (G::post('email') and G::post('email') !== '') {
            $message = 'На сайте http://fotoboom.net/ Вами была заполнена контактная форма:' . "\n\n"
                . '##########################' . "\n"
                . 'Хочу: ' . G::post('action') . "\n"
                . 'Имя: ' . G::post('name') . "\n"
                . 'Email: ' . G::post('email') . "\n"
                . 'Телефон: ' . G::post('phone') . "\n"
                . 'Сообщение: ' . G::post('msg') . "\n"
                . 'Как нашли нас: ' . G::post('found') . "\n"
                . 'Ответить на: ' . G::post('replyTo') . "\n"
                . '##########################' . "\n"
                . 'Мы скоро с Вами свяжемся, не забывайте проверять указанные Вами контакты в форме' . "\n\n"
                . 'Если Вы не обращались к нам - просто проигнорируйте это письмо' . "\n";

            mail(G::post('email'), 'Вы заполнили контактную форму', $message);
        }

        return array(
            'Спасибо за обращение, мы свяжемся с Вами в течении суток',
            false
        );
    }

}
