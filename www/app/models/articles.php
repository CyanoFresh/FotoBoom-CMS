<?php

namespace models;

use core\Model,
    helpers\Cache,
    helpers\G,
    helpers\Filesystem;

class Articles extends Model {

    public function articles() {
        $data = Cache::get('articles');

        if (!$data) {
            $data = $this->db->getAll('SELECT `id`, `date`, `title`, `short`'
                    . ' FROM `articles`'
                    . ' ORDER BY `date` DESC');
            Cache::set('articles', $data);
        }

        return $data;
    }

    public function single($id) {
        $data = Cache::get('article_' . $id);

        if (!$data) {
            $data = $this->db->getRow('SELECT * '
                    . 'FROM `articles` '
                    . 'WHERE `id` = ?i', $id);
            Cache::set('article_' . $id, $data);
        }

        return $data;
    }

    public function add() {
        if ($_FILES['photo']['size'] > 500000) {
            return 'Размер изображения больше <b>500KB</b>';
        }

        $info = $this->db->getRow("SHOW TABLE STATUS LIKE 'articles'");
        $id   = $info['Auto_increment'];

        $dir  = ROOT . '/uploads/articles/';
        $file = $dir . '/' . $id . '.jpg';
        Filesystem::create_dir($dir);

        if (move_uploaded_file($_FILES['photo']['tmp_name'], $file)) {
            $this->db->query('INSERT INTO `articles` '
                    . 'VALUES(NULL, ?i, ?s, ?s, ?s, ?s, ?s)', time(), G::post('title'), $_POST['short'], $_POST['full'], $_POST['description'], G::post('keywords'));

            Cache::delete('articles');

            return 'Статья успешно создана';
        }

        return 'Произошла ошибка записи';
    }

    public function save($id) {
        $id = (int) $id;

        $this->db->query('UPDATE `articles` '
                . 'SET `title` = ?s, '
                . '`short` = ?s, '
                . '`full` = ?s, '
                . '`description` = ?s, '
                . '`keywords` = ?s '
                . 'WHERE `id` = ?i', G::post('title'), $_POST['short'], $_POST['full'], $_POST['description'], G::post('keywords'), $id);

        Cache::delete('article_' . $id);
        Cache::delete('articles');

        if (!empty($_FILES['photo']['name'])) {
            if ($_FILES['photo']['size'] > 500000) {
                return 'Размер изображения больше <b>500KB</b>';
            }

            $dir  = ROOT . '/uploads/articles/';
            $file = $dir . '/' . $id . '.jpg';
            Filesystem::create_dir($dir);

            @unlink($file);

            move_uploaded_file($_FILES['photo']['tmp_name'], $file);
        }

        return 'Статья успешно сохранена';
    }

    public function delete($id) {
        $id = (int) $id;

        $this->db->query('DELETE FROM `articles` WHERE `id` = ?i', $id);

        Cache::delete('article_' . $id);
        Cache::delete('articles');

        return 'Статья успешно удалена';
    }

}
