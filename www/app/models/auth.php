<?php

namespace models;

use core\Model,
    core\App,
    helpers\G,
    helpers\Csrf;

class Auth extends Model {

    public function login() {
        if (!Csrf::valid()) {
            return 'Перезагрузите страницу';
        }

        $login     = G::post('login');
        $pass      = G::post('password');
        $pass_hash = md5($pass . App::$config['system']['sec_code']);

        $data = $this->db->getRow('SELECT `login` '
                . 'FROM `users` '
                . 'WHERE `login` = ?s AND `password` = ?s', $login, $pass_hash);

        if (!$data) {
            return 'Неправильный логин или пароль';
        }

        $_SESSION['login'] = $data['login'];
        return false;
    }

    public function logout() {
        unset($_SESSION['login']);
    }

}
