<?php

namespace models;

use core\Model,
    helpers\Cache,
    helpers\G,
    helpers\Filesystem;

class Prices extends Model {

    public function mainPrices() {
        $data = Cache::get('main_prices');

        if (!$data) {
            $data = $this->db->getAll('SELECT * '
                    . 'FROM `prices` '
                    . 'WHERE `show_main` = 1 '
                    . 'LIMIT 3');
            Cache::set('main_prices', $data);
        }

        return $data;
    }

    public function prices() {
        $data = Cache::get('prices');

        if (!$data) {
            $data = $this->db->getAll('SELECT * FROM `prices`');
            Cache::set('prices', $data);
        }

        return $data;
    }

    public function price($id) {
        $data = Cache::get('price_' . $id);

        if (!$data) {
            $data = $this->db->getRow('SELECT * '
                    . 'FROM `prices` '
                    . 'WHERE `id` = ?i', $id);
            Cache::set('price_' . $id, $data);
        }

        return $data;
    }

    public function add() {
        if ($_FILES['photo']['size'] > 500000) {
            return 'Размер изображения больше <b>500KB</b>';
        }

        if (G::post('show_main') == '1') {
            Cache::delete('main_prices');
            $show_main = 1;
        } else {
            $show_main = 0;
        }

        $info = $this->db->getRow("SHOW TABLE STATUS LIKE 'prices'");
        $id   = $info['Auto_increment'];

        $dir = ROOT . '/uploads/prices/';
        Filesystem::create_dir($dir);

        if (move_uploaded_file($_FILES['photo']['tmp_name'], $dir . '/' . $id . '.jpg')) {
            $this->db->query('INSERT INTO `prices` '
                    . 'VALUES(NULL, ?i, ?s, ?i, ?s, ?s, ?s, ?s)', $show_main, G::post('title'), G::post('price'), G::post('append'), $_POST['body'], $_POST['description'], G::post('keywords'));

            Cache::delete('prices');

            return 'Услуга успешно создана';
        }

        return 'Произошла ошибка записи';
    }

    public function delete($id) {
        $id = (int) $id;

        $this->db->query('DELETE FROM `prices` WHERE `id` = ?i', $id);

        Cache::delete('price_' . $id);
        Cache::delete('prices');
        Cache::delete('main_prices');

        return 'Услуга успешно удалена';
    }

    public function save($id) {
        $id        = (int) $id;
        $show_main = (G::post('show_main') === '1') ? 1 : 0;

        $this->db->query('UPDATE `prices` '
                . 'SET `show_main` = ?i, '
                . '`title` = ?s, '
                . '`price` = ?i, '
                . '`append` = ?s, '
                . '`body` = ?s, '
                . '`description` = ?s, '
                . '`keywords` = ?s '
                . 'WHERE `id` = ?i', $show_main, G::post('title'), G::post('price'), G::post('append'), $_POST['body'], $_POST['description'], G::post('keywords'), $id);

        Cache::delete('price_' . $id);
        Cache::delete('prices');
        Cache::delete('main_prices');

        if (!empty($_FILES['photo']['name'])) {
            if ($_FILES['photo']['size'] > 500000) {
                return 'Размер изображения больше <b>500KB</b>';
            }

            $dir  = ROOT . '/uploads/prices/';
            Filesystem::create_dir($dir);
            $file = $dir . '/' . $id . '.jpg';

            @unlink($file);

            move_uploaded_file($_FILES['photo']['tmp_name'], $file);
        }

        return 'Услуга успешно сохранена';
    }

}
