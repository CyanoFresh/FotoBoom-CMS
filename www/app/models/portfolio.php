<?php

namespace models;

use core\Model,
    helpers\Cache,
    helpers\G,
    helpers\Filesystem;

class Portfolio extends Model {

    public function albums() {
        $data = Cache::get('albums');

        if (!$data) {
            $data = $this->db->getAll('SELECT `id`, `title`, `description` '
                    . 'FROM `albums`');
            Cache::set('albums', $data);
        }

        return $data;
    }

    public function mainAlbums() {
        $data = Cache::get('main_albums');

        if (!$data) {
            $data = $this->db->getAll('SELECT `id`, `title` '
                    . 'FROM `albums` '
                    . 'WHERE `show_main` = 1 '
                    . 'LIMIT 6');
            Cache::set('main_albums', $data);
        }

        return $data;
    }

    public function album($id) {
        $id = (int) $id;
        $data = Cache::get('album_' . $id);

        if (!$data) {
            $data = $this->db->getRow('SELECT * '
                    . 'FROM `albums` '
                    . 'WHERE `id` = ?i', $id);
            Cache::set('album_' . $id, $data);
        }

        return $data;
    }

    public function addAlbum() {
        if ($_FILES['preview']['size'] > 500000) {
            return 'Размер изображения больше <b>500KB</b>';
        }

        $show_main = (G::post('show_main') === '1') ? 1 : 0;

        $info = $this->db->getRow("SHOW TABLE STATUS LIKE 'albums'");
        $id = $info['Auto_increment'];

        $dir = ROOT . '/uploads/albums/' . $id;
        Filesystem::create_dir($dir);

        if (move_uploaded_file($_FILES['preview']['tmp_name'], $dir . '/preview.jpg')) {
            $this->db->query('INSERT INTO `albums` '
                    . 'VALUES(?i, ?i, ?s, ?s, ?s, ?s)', $id, $show_main, G::post('title'), $_POST['body'], $_POST['description'], G::post('keywords'));

            Cache::delete('albums');
            Cache::delete('main_albums');

            return 'Альбом успешно создан';
        }

        return 'Произошла ошибка записи';
    }

    public function saveAlbum($id) {
        $id = (int) $id;
        $show_main = (G::post('show_main') === '1') ? 1 : 0;

        $this->db->query('UPDATE `albums` '
                . 'SET `show_main` = ?i, '
                . '`title` = ?s, '
                . '`body` = ?s, '
                . '`description` = ?s, '
                . '`keywords` = ?s '
                . 'WHERE `id` = ?i', $show_main, G::post('title'), $_POST['body'], $_POST['description'], G::post('keywords'), $id);

        Cache::delete('albums');
        Cache::delete('main_albums');
        Cache::delete('album_' . $id);

        if (!empty($_FILES['preview']['name'])) {
            if ($_FILES['preview']['size'] > 500000) {
                return 'Размер изображения больше <b>500KB</b>';
            }

            $dir = ROOT . '/uploads/albums/' . $id;
            $file = $dir . '/preview.jpg';
            Filesystem::create_dir($dir);

            @unlink($file);

            move_uploaded_file($_FILES['preview']['tmp_name'], $file);
        }

        return 'Альбом успешно сохранен';
    }

    public function delAlbum($id) {
        $id = (int) $id;

        $this->db->query('DELETE FROM `albums` WHERE `id` = ?i', $id);
        $this->db->query('DELETE FROM `photos` WHERE `album_id` = ?i', $id);

        Cache::delete('albums');
        Cache::delete('main_albums');
        Cache::delete('album_' . $id);
        Cache::delete('photos_' . $id);

        Filesystem::delete(ROOT . '/uploads/albums/' . $id, true);

        return 'Альбом успешно удален';
    }

    public function getPhotos($id) {
        $id = (int) $id;
        $data = Cache::get('photos_' . $id);

        if (!$data) {
            $data = $this->db->getAll('SELECT * '
                    . 'FROM `photos` '
                    . 'WHERE `album_id` = ?i '
                    . 'ORDER BY `listorder`', $id);
            Cache::set('photos_' . $id, $data);
        }

        return $data;
    }

    public function savePhoto() {
        $this->db->query('UPDATE `photos` '
                . 'SET `title` = ?s '
                . 'WHERE `id` = ?i', G::post('value'), G::post('pk'));
        Cache::delete('photos_' . G::post('name'));
    }

    public function dropPhoto($album) {
        // Photo data
        $info = $this->db->getRow("SHOW TABLE STATUS LIKE 'photos'");
        $id = $info['Auto_increment'];

        $this->db->query('INSERT INTO `photos` '
                . 'VALUES(NULL, ?i, NULL, NULL)', $album);

        // Watermark
        $temp = $_FILES['file']['tmp_name'];

        $watermark = imagecreatefrompng(ROOT . '/theme/assets/img/watermark.png');
        $watermark_width = imagesx($watermark);
        $watermark_height = imagesy($watermark);

        $photo = imagecreatefromjpeg($temp);
        $size = getimagesize($temp);

        $dest_x = $size[0] - $watermark_width;
        $dest_y = $size[1] - $watermark_height;

        imagealphablending($photo, true);
        imagealphablending($watermark, true);

        $file = ROOT . '/uploads/albums/' . $album . '/' . $id . '.jpg';

        imagecopy($photo, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height);
        imagejpeg($photo, $file, 100);

        unlink($temp);
        imagedestroy($photo);
        Cache::delete('photos_' . $album);

        return array(
            'id' => $id,
        );
    }

    public function delPhoto($album) {
        $album = (int) $album;

        $this->db->query('DELETE FROM `photos` '
                . 'WHERE `id` = ?i', G::post('id'));

        @unlink(ROOT . '/uploads/albums/' . $album . '/' . G::post('id') . '.jpg');

        Cache::delete('photos_' . $album);
    }

    public function order($id) {
        $id = (int) $id;
        $data = $_POST['photo'];

        if ($data) {
            $i = 1;
            foreach ($data as $photo) {
                $this->db->query('UPDATE `photos` SET `listorder` = ?i WHERE `id` = ?i', $i, $photo);
                $i++;
            }
            Cache::delete('photos_' . $id);
        }
    }

}
