<?php

namespace models;

use core\Model,
    helpers\G,
    helpers\Filesystem;

class Dashboard extends Model {

    public function save() {
        $cfg    = $_POST['cfg'];
        $config = APP . '/config/settings.php';

        $handle = fopen($config, 'w');
        fwrite($handle, "<?php \$settings=array(\n");

        foreach ($cfg as $key => $value) {
            fwrite($handle, "    '$key' => '$value',\n");
        }

        fwrite($handle, ');');

        return 'Настройки успешно сохранены';
    }

    public function addAdmin() {
        $pass = md5(G::post('pass') . \core\App::$config['system']['sec_code']);

        $this->db->query('INSERT INTO `users` '
                . 'VALUES(NULL, ?s, ?s, ?s)', G::post('login'), $pass, G::post('fullname'));

        return 'Вы успешно добавили нового администратора';
    }

    public function cache() {
        Filesystem::delete(APP . '/tmp/cache', true);
        return 'Кеш успешно удален';
    }

}
