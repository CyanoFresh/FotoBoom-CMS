<?php

namespace models;

use core\Model,
    helpers\Cache,
    helpers\G,
    helpers\Filesystem;

class Reviews extends Model {

    public function reviews() {
        $data = Cache::get('reviews');

        if (!$data) {
            $data = $this->db->getAll('SELECT * FROM `reviews`');
            Cache::set('reviews', $data);
        }

        return $data;
    }

    public function addReview() {
        $info = $this->db->getRow("SHOW TABLE STATUS LIKE 'reviews'");
        $id   = $info['Auto_increment'];

        $dir = ROOT . '/uploads/reviews/';
        Filesystem::create_dir($dir);

        if (move_uploaded_file($_FILES['photo']['tmp_name'], $dir . '/' . $id . '.jpg')) {
            $this->db->query('INSERT INTO `reviews` '
                    . 'VALUES(NULL, ?s, ?s)', G::post('author'), G::post('review'));

            Cache::delete('reviews');
        }
    }

}