<?php

namespace helpers;

use core\App;

class Csrf {

    public static function token() {
        $token = md5(microtime() . App::$config['system']['sec_code']);
        if (empty($_SESSION['tokens'])) {
            $_SESSION['tokens'] = array();
        }
        if (count($_SESSION['tokens']) > 1) {
            unset($_SESSION['tokens']);
        }
        $_SESSION['tokens'][$token] = true;
        return $token;
    }

    public static function valid() {
        $input_token = G::post('token');

        if (isset($_SESSION['tokens'][$input_token])) {
            unset($_SESSION['tokens'][$input_token]);
            return true;
        }
        return false;
    }

}
