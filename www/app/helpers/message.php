<?php

namespace helpers;

class Message {

    public static function get() {
        $msg = G::session('message');
        self::clear();

        return $msg;
    }

    public static function set($message) {
        $_SESSION['message'] = $message;
    }

    public static function clear() {
        unset($_SESSION['message']);
    }

}
