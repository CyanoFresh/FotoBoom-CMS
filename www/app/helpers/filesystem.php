<?php

namespace helpers;

/**
 * Filesystem util class
 *
 * This class contains function to work with filesystem easly
 *
 * @author AlexMerser
 * @category Utils
 */
class Filesystem {

    /**
     * Function to delete file or dir
     *
     * @param string $path Path to delete
     * @param boolean $including Including last folder to delete
     * @return boolean
     */
    public static function delete($path, $including = false) {
        $files = glob($path . '/*');
        if ($files) {
            foreach ($files as $object) {
                if (is_dir($object)) {
                    self::delete($object, $including);
                } else {
                    unlink($object);
                }
            }
        }

        if ($including) {
            rmdir($path);
        }

        return true;
    }

    /**
     * Function to create folder
     *
     * @param string $dir Path to create
     * @return boolean
     */
    public static function create_dir($dir) {
        /* Check if directory exists */
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
            return true;
        }

        /* If path exists - return false */
        return false;
    }

}
