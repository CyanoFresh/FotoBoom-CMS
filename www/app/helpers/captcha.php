<?php

namespace helpers;

class Captcha {

    public static function html() {
        require_once APP . '/libs/recaptchalib.php';
        $publickey = \core\App::$config['system']['publickey'];

        return recaptcha_get_html($publickey);
    }

    public static function valid() {
        require_once APP . '/libs/recaptchalib.php';
        $privatekey = \core\App::$config['system']['privatekey'];

        $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

        return (bool) $resp->is_valid;
    }

}
