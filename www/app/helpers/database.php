<?php

namespace helpers;

use libs\SafeMySQL;

class Database extends SafeMySQL {

    private static $_instance;

    public static function getInstance($options = array()) {
        if (is_null(self::$_instance)) {
            self::$_instance = new self($options);
        }
        return self::$_instance;
    }

}
