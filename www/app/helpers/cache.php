<?php

namespace helpers;

class Cache {

    public static function get($file) {
        $file = self::getFile($file);

        if (!self::valid($file)) {
            return false;
        }

        $handle = fopen($file, 'rb');
        $data   = fread($handle, filesize($file));
        fclose($handle);

        return unserialize($data);
    }

    public static function set($file, $data) {
        $file = self::getFile($file);

        $handle = fopen($file, 'wb');
        fwrite($handle, serialize($data));
        fclose($handle);

        return true;
    }

    public static function valid($file) {
        if (is_readable($file) AND is_writable($file)) {
            return true;
        }
        return false;
    }

    public static function delete($file) {
        $file = self::getFile($file);

        @unlink($file);

        return false;
    }

    private static function getFile($file) {
        $cache_dir = APP . '/tmp/cache/content/';
        Filesystem::create_dir($cache_dir);

        $file = $cache_dir . $file . '.cache';
        return $file;
    }

}
