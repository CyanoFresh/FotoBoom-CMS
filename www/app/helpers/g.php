<?php

namespace helpers;

class G {

    public static function get($var) {
        if (!isset($_GET[$var])) {
            return false;
        }

        $var = filter_input(INPUT_GET, $var, FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        return $var;
    }

    public static function post($var) {
        if (!isset($_POST[$var])) {
            return false;
        }

        $var = filter_input(INPUT_POST, $var, FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        return $var;
    }

    public static function session($var) {
        if (!isset($_SESSION[$var])) {
            return false;
        }

        $var = filter_var($_SESSION[$var], FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        return $var;
    }

}
